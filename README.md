# Movie project

Welcome to my Movie website and booking ticket .

### TaskTable

https://docs.google.com/spreadsheets/d/1M7qsv3arlGaRMPjLEA3i27Wao_ulIDBMHjMI46uTvaA/edit?usp=sharing

### WireFrames

https://drive.google.com/drive/folders/1DlNYrHUKYOqh3Y8lBnA1koeTL2xg1AA1?usp=share_link

### Deployment

https://capstonereactjs-movie-git-master-move-ticket-vnc.vercel.app/

## Requests

Data base from list user GP01

- You cannot access the site AdminPage without an admin account.
  my account : vnc98 password:1234

## Install

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### Third Party libraries used except for React and Tailwind

- [react-rounter-dom](https://reactrouter.com/en/main)
- [formik](https://formik.org/)
- [axios](https://github.com/axios/axios)
- [antd](https://ant.design/)
- [react-slick](https://react-slick.neostack.com/)
- [react-redux](https://react-redux.js.org/)
- [redux-thunk](https://github.com/reduxjs/redux-thunk)
- [react-spinners](https://github.com/davidhu2000/react-spinners)
- [lottie-react](https://github.com/Gamote/lottie-react)
