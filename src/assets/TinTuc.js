export let TinTuc = [
  {
    url: "https://ghienreview.com/review-phim-avatar-2-mau-sac-ao-dieu-cua-thien-duong/",
    title: "Review phim Avatar 2: Màu sắc ảo diệu của thiên đường",
    detail:
      "Sau 13 năm, bộ phim có doanh thu cao nhất mọi thời đại mới công chiếu phần 2 và ngay lập tức Avatar 2 – Dòng chảy của nước đã tạo nên một cơn sốt trên khắp các phòng vé toàn cầu. Ở Việt Nam, rất khó để các bạn có thể book được một ghế có chỗ ngồi đẹp, thậm chí là book được 1 ghế tại một rạp phim bất kỳ vào khung giờ đẹp của bộ phim này, cho thấy sức hút khủng khiếp của Avatar lớn đến thế nào.",
    img: "https://ghienreview.com/wp-content/uploads/2022/12/17D6B5F9-6A7B-429F-92DD-2A3B588955B6.jpeg",
  },
  {
    url: "https://ghienreview.com/review-phim-thanh-guom-diet-quy-xem-xong-phai-tim-truyen-doc-lien/",
    title: "Thanh gươm diệt quỷ: Xem xong phải tìm truyện đọc liền",
    detail:
      "Bom tấn Anime đang làm mưa làm gió ở Nhật Bản và Đài Loan, đổ xô mọi kỷ lục phòng vé và là đối thủ nặng ký nhất của Khương Tử Nha (2020) ở vị trí phim hoạt hình ăn khách nhất năm 2020 đã chính thức được công chiếu tại các rạp ở Việt Nam.",
    img: "https://ghienreview.com/wp-content/uploads/2020/12/Ghien-review-Thanh-guom-diet-quy-Demon-Slayer-02-min.jpg",
  },
  {
    url: "https://khenphim.com/babylon/",
    title: "Babylon – Bức tranh đen tối nhưng chân thật về Hollywood một thời",
    detail:
      "Babylon lấy bối cảnh những năm 1920, khi Jack Conard (Brad Pitt vào vai) còn là nam diễn viên hạng A mà nhiều nhà sản xuất thèm khát, cũng như được khán giả mến mộ. Cùng thời điểm ấy, Nellie LaRoy (Margot Robbie) bắt đầu nổi lên như một nữ diễn viên triển vọng với khả năng diễn xuất tốt, kéo theo đó là anh chàng Manny Torres (Diego Calva) dần lấn sân vào thế giới điện ảnh dù trước đó chỉ là một anh chàng nài voi ít ai biết tới",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/02/Babylon-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/nha-ba-nu/",
    title: "Nhà Bà Nữ – Trọn vẹn cảm xúc, chạm tới trái tim khán giả",
    detail:
      "Nhà Bà Nữ dĩ nhiên là câu chuyện xoay quanh bà Nữ (Lê Giang vào vai) – một người phụ nữ bán bánh canh nhưng giọng chua hơn chanh và là người cai quản mọi việc lớn nhỏ trong nhà. Cái gia đình bất ổn một cách ổn định cho đến khi Nhi (Uyển Ân) có bạn trai, cũng như Như (Khả Như) và chồng (Trấn Thành) có những mâu thuẫn.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Nha-Ba-Nu-3-Le-Giang_KP.webp",
  },
  {
    url: "https://khenphim.com/where-the-crawdads-sing/",
    title: "Xa Ngoài Kia Nơi Loài Tôm Hát: Dữ dội như sóng ngầm",
    detail:
      "Xa Ngoài Kia Nơi Loài Tôm Hát (tên gốc: Where The Crawdads Sing) thu hút khán giả bởi phim được chuyển thể từ cuốn tiểu thuyết cùng tên (tác giả Delia Owens) cũng như có bài hát gốc của Taylor Swift.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/08/Where-The-Crawdads-Sing-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/black-adam/",
    title: "Black Adam: Đẹp mắt, đã tai và nội dung bất ngờ",
    detail:
      "Black Adam kể về một nhân vật cùng tên của nhà DC. Dwayne Johnson hay còn được biết đến dưới cái tên The Rock sẽ thủ vai chính, dưới sự sáng tạo của đạo diễn Jaume Collet-Serra.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/10/Black-Adam-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/jailangkung-sandekala/",
    title: "Jailangkung: Sandekala",
    detail:
      "Jailangkung: Búp Bê Gọi Hồn kể về một gia đình nọ gồm 4 người: vợ, chồng, con gái, con trai út. Trong chuyến đi chơi cạnh hồ nước, cậu con út bị lạc vào rừng rồi mất tích một cách bí ẩn. Dù huy động cả một lực lượng tìm kiếm chuyên nghiệp nhưng vẫn không thể tìm ra tung tích cậu bé. Người dân địa phương đồn đoán rằng khu vực quanh hồ bị ám, đã từng có rất nhiều trẻ em mất tích...",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Jailangkung-Sandekala-3_KP.webp",
  },
  {
    url: "https://khenphim.com/2037/",
    title: "Điều Ước Cuối Của Tù Nhân 2037: Một cái kết ẩn ý",
    detail:
      "Điều Ước Cuối Của Tù Nhân 2037 (이공삼칠) sở hữu câu chuyện không phải mới lạ, nhưng kỳ ở chỗ là nó vẫn cuốn hút, mang đến cảm giác dằn vặt khó tả, y như thứ cảm xúc mà nhân vật Yoon-yeong phải vượt qua",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/07/2037-3_KP-1920x1280.webp",
  },
  {
    url: "https://khenphim.com/trailer-the-matrix-resurrections/",
    title: "Huyền thoại “Ma Trận” hồi sinh",
    detail:
      "Gần hai thập kỷ sau khi bộ ba phim Ma Trận khép lại với sự ra đi của cặp đôi Neo (Keanu Reeves) – Trinity (Carrie-Anne Moss), đạo diễn Lana Wachowski đã quyết định mang thương hiệu thành công bậc nhất những năm đầu đầu thế kỷ 21 trở lại màn ảnh rộng.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2021/09/THE-MATRIX-RESURRECTIONS-1-teaser-poster.jpg",
  },
  {
    url: "https://khenphim.com/chi-chi-em-em-2/",
    title:
      "Chị Chị Em Em 2: Khi mỹ nhân lên tiếng, đàn ông sẽ quỳ rạp dưới chân",
    detail:
      "Chị Chị Em Em 2 có sự xuất hiện của Minh Hằng và Ngọc Trinh lần lượt trong vai Ba Trà và Tư Nhị đã khiến khán giả lập tức thắc mắc rằng liệu những người đẹp này có đủ sức làm nên thành công về mặt diễn xuất cho bộ phim hay không? Với những giai thoại đã biết về hai mỹ nhân nức tiếng Sài Thành đầu thế kỷ 20.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/02/Chi-Chi-Em-Em-2-2_KP.webp",
  },
  {
    url: "https://khenphim.com/take-care-of-my-mom/",
    title: "Hãy Chăm Sóc Mẹ: Lời nhắc nhở cho những ai còn mẹ",
    detail:
      "Hãy Chăm Sóc Mẹ xoay quanh một cụ bà 85 tuổi sống một mình ở vùng Daegu, Hàn Quốc. Bằng cách cho thuê nhà, bà vẫn có đồng ra đồng vào để tự do tiêu xài mà không cần nhờ đến sự trợ giúp của con cháu.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Take-Care-Of-My-Mom-4_KP.webp",
  },
  {
    url: "https://khenphim.com/strange-world/",
    title:
      "Strange World (Thế Giới Lạ Lùng): 3D và đồ họa đẹp, nội dung dễ hiểu",
    detail:
      "Strange World (Thế Giới Lạ Lùng) xoay quanh gia đình Clades với những thành viên đã làm nên huyền thoại trong làng phiêu lưu khám phá những vùng đất mới..",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Strange-World-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/puss-in-boots-the-last-wish/",
    title: "Review phim Mèo Đi Hia: Điều Ước Cuối Cùng",
    detail:
      "Mèo Đi Hia: Điều Ước Cuối Cùng (tên gốc: Puss In Boots: The Last Wish) kể về chuyến phiêu lưu đi tìm một ngôi sao có khả năng đặc biệt, nó sẽ ban cho bất kỳ ai một điều ước, bất cứ thứ gì mà họ nói ra sẽ thành hiện thực ngay..",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Puss-In-Boots-2022-2-Perrito_KP-e1671923386998.webp",
  },
  {
    url: "https://khenphim.com/operation-fortune/",
    title: " Phi Vụ Toàn Sao: Hành động đã mắt, tấu hài cực mạnh'",
    detail:
      "Phi Vụ Toàn Sao xoay quanh một phi vụ của điệp viên Orson Fortune (Jason Statham) cùng đồng đội lên kế hoạch bắt tận tay một gã chuyên buôn vũ khí trong thế giới ngầm.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Operation-Fortune_2.jpg",
  },
  {
    url: "https://khenphim.com/doctor-strange-in-the-multiverse-of-madness/",
    title:
      "Review phim Doctor Strange Phù Thủy Tối Thượng Trong Đa Vũ Trụ Hỗn Loạn",
    detail:
      "Doctor Strange vô tình sử dụng một phép thuật cấm, mở ra cánh cổng đa vũ trụ, khiến Strange phải đối mặt với một kẻ thù vừa lạ vừa quen, người đó tìm cách tiêu diệt các phù thủy để hòng đạt được mục tiêu cá nhân của mình.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/05/Doctor-Strange-madness-2-Dr-Strange_KP.webp",
  },
  {
    url: "https:www.zingnews.vn//lien-hoan-phim-busan-nong-vi-avatar-2-post1362665.html",
    title: "Liên hoan phim Busan nóng vì ‘Avatar 2’",
    detail:
      "Khán giả LHP Busan đã trả tiền để mua kính 3D và xem clip công chiếu sớm bom tấn lớn nhất 2022 của đạo diễn James Cameron.",
    img: "https://znews-photo.zingcdn.me/Uploaded/rohunaa/2022_10_06/wallpapersden.com_avatar_2_metkayina_village_2048x1113_1.jpg",
  },
  {
    url: "https://ghienreview.com/review-phim-avatar-2-mau-sac-ao-dieu-cua-thien-duong/",
    title: "Review phim Avatar 2: Màu sắc ảo diệu của thiên đường",
    detail:
      "Sau 13 năm, bộ phim có doanh thu cao nhất mọi thời đại mới công chiếu phần 2 và ngay lập tức Avatar 2 – Dòng chảy của nước đã tạo nên một cơn sốt trên khắp các phòng vé toàn cầu. Ở Việt Nam, rất khó để các bạn có thể book được một ghế có chỗ ngồi đẹp, thậm chí là book được 1 ghế tại một rạp phim bất kỳ vào khung giờ đẹp của bộ phim này, cho thấy sức hút khủng khiếp của Avatar lớn đến thế nào.",
    img: "https://ghienreview.com/wp-content/uploads/2022/12/17D6B5F9-6A7B-429F-92DD-2A3B588955B6.jpeg",
  },
  {
    url: "https://ghienreview.com/review-phim-thanh-guom-diet-quy-xem-xong-phai-tim-truyen-doc-lien/",
    title: "Thanh gươm diệt quỷ: Xem xong phải tìm truyện đọc liền",
    detail:
      "Bom tấn Anime đang làm mưa làm gió ở Nhật Bản và Đài Loan, đổ xô mọi kỷ lục phòng vé và là đối thủ nặng ký nhất của Khương Tử Nha (2020) ở vị trí phim hoạt hình ăn khách nhất năm 2020 đã chính thức được công chiếu tại các rạp ở Việt Nam.",
    img: "https://ghienreview.com/wp-content/uploads/2020/12/Ghien-review-Thanh-guom-diet-quy-Demon-Slayer-02-min.jpg",
  },
  {
    url: "https://khenphim.com/babylon/",
    title: "Babylon – Bức tranh đen tối nhưng chân thật về Hollywood một thời",
    detail:
      "Babylon lấy bối cảnh những năm 1920, khi Jack Conard (Brad Pitt vào vai) còn là nam diễn viên hạng A mà nhiều nhà sản xuất thèm khát, cũng như được khán giả mến mộ. Cùng thời điểm ấy, Nellie LaRoy (Margot Robbie) bắt đầu nổi lên như một nữ diễn viên triển vọng với khả năng diễn xuất tốt, kéo theo đó là anh chàng Manny Torres (Diego Calva) dần lấn sân vào thế giới điện ảnh dù trước đó chỉ là một anh chàng nài voi ít ai biết tới",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/02/Babylon-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/nha-ba-nu/",
    title: "Nhà Bà Nữ – Trọn vẹn cảm xúc, chạm tới trái tim khán giả",
    detail:
      "Nhà Bà Nữ dĩ nhiên là câu chuyện xoay quanh bà Nữ (Lê Giang vào vai) – một người phụ nữ bán bánh canh nhưng giọng chua hơn chanh và là người cai quản mọi việc lớn nhỏ trong nhà. Cái gia đình bất ổn một cách ổn định cho đến khi Nhi (Uyển Ân) có bạn trai, cũng như Như (Khả Như) và chồng (Trấn Thành) có những mâu thuẫn.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Nha-Ba-Nu-3-Le-Giang_KP.webp",
  },
  {
    url: "https://khenphim.com/where-the-crawdads-sing/",
    title: "Xa Ngoài Kia Nơi Loài Tôm Hát: Dữ dội như sóng ngầm",
    detail:
      "Xa Ngoài Kia Nơi Loài Tôm Hát (tên gốc: Where The Crawdads Sing) thu hút khán giả bởi phim được chuyển thể từ cuốn tiểu thuyết cùng tên (tác giả Delia Owens) cũng như có bài hát gốc của Taylor Swift.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/08/Where-The-Crawdads-Sing-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/black-adam/",
    title: "Black Adam: Đẹp mắt, đã tai và nội dung bất ngờ",
    detail:
      "Black Adam kể về một nhân vật cùng tên của nhà DC. Dwayne Johnson hay còn được biết đến dưới cái tên The Rock sẽ thủ vai chính, dưới sự sáng tạo của đạo diễn Jaume Collet-Serra.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/10/Black-Adam-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/jailangkung-sandekala/",
    title: "Jailangkung: Sandekala",
    detail:
      "Jailangkung: Búp Bê Gọi Hồn kể về một gia đình nọ gồm 4 người: vợ, chồng, con gái, con trai út. Trong chuyến đi chơi cạnh hồ nước, cậu con út bị lạc vào rừng rồi mất tích một cách bí ẩn. Dù huy động cả một lực lượng tìm kiếm chuyên nghiệp nhưng vẫn không thể tìm ra tung tích cậu bé. Người dân địa phương đồn đoán rằng khu vực quanh hồ bị ám, đã từng có rất nhiều trẻ em mất tích...",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Jailangkung-Sandekala-3_KP.webp",
  },
  {
    url: "https://khenphim.com/2037/",
    title: "Điều Ước Cuối Của Tù Nhân 2037: Một cái kết ẩn ý",
    detail:
      "Điều Ước Cuối Của Tù Nhân 2037 (이공삼칠) sở hữu câu chuyện không phải mới lạ, nhưng kỳ ở chỗ là nó vẫn cuốn hút, mang đến cảm giác dằn vặt khó tả, y như thứ cảm xúc mà nhân vật Yoon-yeong phải vượt qua",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/07/2037-3_KP-1920x1280.webp",
  },
  {
    url: "https://khenphim.com/trailer-the-matrix-resurrections/",
    title: "Huyền thoại “Ma Trận” hồi sinh",
    detail:
      "Gần hai thập kỷ sau khi bộ ba phim Ma Trận khép lại với sự ra đi của cặp đôi Neo (Keanu Reeves) – Trinity (Carrie-Anne Moss), đạo diễn Lana Wachowski đã quyết định mang thương hiệu thành công bậc nhất những năm đầu đầu thế kỷ 21 trở lại màn ảnh rộng.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2021/09/THE-MATRIX-RESURRECTIONS-1-teaser-poster.jpg",
  },
  {
    url: "https://khenphim.com/chi-chi-em-em-2/",
    title:
      "Chị Chị Em Em 2: Khi mỹ nhân lên tiếng, đàn ông sẽ quỳ rạp dưới chân",
    detail:
      "Chị Chị Em Em 2 có sự xuất hiện của Minh Hằng và Ngọc Trinh lần lượt trong vai Ba Trà và Tư Nhị đã khiến khán giả lập tức thắc mắc rằng liệu những người đẹp này có đủ sức làm nên thành công về mặt diễn xuất cho bộ phim hay không? Với những giai thoại đã biết về hai mỹ nhân nức tiếng Sài Thành đầu thế kỷ 20.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/02/Chi-Chi-Em-Em-2-2_KP.webp",
  },
  {
    url: "https://khenphim.com/take-care-of-my-mom/",
    title: "Hãy Chăm Sóc Mẹ: Lời nhắc nhở cho những ai còn mẹ",
    detail:
      "Hãy Chăm Sóc Mẹ xoay quanh một cụ bà 85 tuổi sống một mình ở vùng Daegu, Hàn Quốc. Bằng cách cho thuê nhà, bà vẫn có đồng ra đồng vào để tự do tiêu xài mà không cần nhờ đến sự trợ giúp của con cháu.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Take-Care-Of-My-Mom-4_KP.webp",
  },
  {
    url: "https://khenphim.com/strange-world/",
    title:
      "Strange World (Thế Giới Lạ Lùng): 3D và đồ họa đẹp, nội dung dễ hiểu",
    detail:
      "Strange World (Thế Giới Lạ Lùng) xoay quanh gia đình Clades với những thành viên đã làm nên huyền thoại trong làng phiêu lưu khám phá những vùng đất mới..",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Strange-World-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/puss-in-boots-the-last-wish/",
    title: "Review phim Mèo Đi Hia: Điều Ước Cuối Cùng",
    detail:
      "Mèo Đi Hia: Điều Ước Cuối Cùng (tên gốc: Puss In Boots: The Last Wish) kể về chuyến phiêu lưu đi tìm một ngôi sao có khả năng đặc biệt, nó sẽ ban cho bất kỳ ai một điều ước, bất cứ thứ gì mà họ nói ra sẽ thành hiện thực ngay..",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Puss-In-Boots-2022-2-Perrito_KP-e1671923386998.webp",
  },
  {
    url: "https://khenphim.com/operation-fortune/",
    title: " Phi Vụ Toàn Sao: Hành động đã mắt, tấu hài cực mạnh'",
    detail:
      "Phi Vụ Toàn Sao xoay quanh một phi vụ của điệp viên Orson Fortune (Jason Statham) cùng đồng đội lên kế hoạch bắt tận tay một gã chuyên buôn vũ khí trong thế giới ngầm.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Operation-Fortune_2.jpg",
  },
  {
    url: "https://khenphim.com/doctor-strange-in-the-multiverse-of-madness/",
    title:
      "Review phim Doctor Strange Phù Thủy Tối Thượng Trong Đa Vũ Trụ Hỗn Loạn",
    detail:
      "Doctor Strange vô tình sử dụng một phép thuật cấm, mở ra cánh cổng đa vũ trụ, khiến Strange phải đối mặt với một kẻ thù vừa lạ vừa quen, người đó tìm cách tiêu diệt các phù thủy để hòng đạt được mục tiêu cá nhân của mình.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/05/Doctor-Strange-madness-2-Dr-Strange_KP.webp",
  },
  {
    url: "https:www.zingnews.vn//lien-hoan-phim-busan-nong-vi-avatar-2-post1362665.html",
    title: "Liên hoan phim Busan nóng vì ‘Avatar 2’",
    detail:
      "Khán giả LHP Busan đã trả tiền để mua kính 3D và xem clip công chiếu sớm bom tấn lớn nhất 2022 của đạo diễn James Cameron.",
    img: "https://znews-photo.zingcdn.me/Uploaded/rohunaa/2022_10_06/wallpapersden.com_avatar_2_metkayina_village_2048x1113_1.jpg",
  },
  {
    url: "https://ghienreview.com/review-phim-avatar-2-mau-sac-ao-dieu-cua-thien-duong/",
    title: "Review phim Avatar 2: Màu sắc ảo diệu của thiên đường",
    detail:
      "Sau 13 năm, bộ phim có doanh thu cao nhất mọi thời đại mới công chiếu phần 2 và ngay lập tức Avatar 2 – Dòng chảy của nước đã tạo nên một cơn sốt trên khắp các phòng vé toàn cầu. Ở Việt Nam, rất khó để các bạn có thể book được một ghế có chỗ ngồi đẹp, thậm chí là book được 1 ghế tại một rạp phim bất kỳ vào khung giờ đẹp của bộ phim này, cho thấy sức hút khủng khiếp của Avatar lớn đến thế nào.",
    img: "https://ghienreview.com/wp-content/uploads/2022/12/17D6B5F9-6A7B-429F-92DD-2A3B588955B6.jpeg",
  },
  {
    url: "https://ghienreview.com/review-phim-thanh-guom-diet-quy-xem-xong-phai-tim-truyen-doc-lien/",
    title: "Thanh gươm diệt quỷ: Xem xong phải tìm truyện đọc liền",
    detail:
      "Bom tấn Anime đang làm mưa làm gió ở Nhật Bản và Đài Loan, đổ xô mọi kỷ lục phòng vé và là đối thủ nặng ký nhất của Khương Tử Nha (2020) ở vị trí phim hoạt hình ăn khách nhất năm 2020 đã chính thức được công chiếu tại các rạp ở Việt Nam.",
    img: "https://ghienreview.com/wp-content/uploads/2020/12/Ghien-review-Thanh-guom-diet-quy-Demon-Slayer-02-min.jpg",
  },
  {
    url: "https://khenphim.com/babylon/",
    title: "Babylon – Bức tranh đen tối nhưng chân thật về Hollywood một thời",
    detail:
      "Babylon lấy bối cảnh những năm 1920, khi Jack Conard (Brad Pitt vào vai) còn là nam diễn viên hạng A mà nhiều nhà sản xuất thèm khát, cũng như được khán giả mến mộ. Cùng thời điểm ấy, Nellie LaRoy (Margot Robbie) bắt đầu nổi lên như một nữ diễn viên triển vọng với khả năng diễn xuất tốt, kéo theo đó là anh chàng Manny Torres (Diego Calva) dần lấn sân vào thế giới điện ảnh dù trước đó chỉ là một anh chàng nài voi ít ai biết tới",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/02/Babylon-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/nha-ba-nu/",
    title: "Nhà Bà Nữ – Trọn vẹn cảm xúc, chạm tới trái tim khán giả",
    detail:
      "Nhà Bà Nữ dĩ nhiên là câu chuyện xoay quanh bà Nữ (Lê Giang vào vai) – một người phụ nữ bán bánh canh nhưng giọng chua hơn chanh và là người cai quản mọi việc lớn nhỏ trong nhà. Cái gia đình bất ổn một cách ổn định cho đến khi Nhi (Uyển Ân) có bạn trai, cũng như Như (Khả Như) và chồng (Trấn Thành) có những mâu thuẫn.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Nha-Ba-Nu-3-Le-Giang_KP.webp",
  },
  {
    url: "https://khenphim.com/where-the-crawdads-sing/",
    title: "Xa Ngoài Kia Nơi Loài Tôm Hát: Dữ dội như sóng ngầm",
    detail:
      "Xa Ngoài Kia Nơi Loài Tôm Hát (tên gốc: Where The Crawdads Sing) thu hút khán giả bởi phim được chuyển thể từ cuốn tiểu thuyết cùng tên (tác giả Delia Owens) cũng như có bài hát gốc của Taylor Swift.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/08/Where-The-Crawdads-Sing-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/black-adam/",
    title: "Black Adam: Đẹp mắt, đã tai và nội dung bất ngờ",
    detail:
      "Black Adam kể về một nhân vật cùng tên của nhà DC. Dwayne Johnson hay còn được biết đến dưới cái tên The Rock sẽ thủ vai chính, dưới sự sáng tạo của đạo diễn Jaume Collet-Serra.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/10/Black-Adam-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/jailangkung-sandekala/",
    title: "Jailangkung: Sandekala",
    detail:
      "Jailangkung: Búp Bê Gọi Hồn kể về một gia đình nọ gồm 4 người: vợ, chồng, con gái, con trai út. Trong chuyến đi chơi cạnh hồ nước, cậu con út bị lạc vào rừng rồi mất tích một cách bí ẩn. Dù huy động cả một lực lượng tìm kiếm chuyên nghiệp nhưng vẫn không thể tìm ra tung tích cậu bé. Người dân địa phương đồn đoán rằng khu vực quanh hồ bị ám, đã từng có rất nhiều trẻ em mất tích...",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Jailangkung-Sandekala-3_KP.webp",
  },
  {
    url: "https://khenphim.com/2037/",
    title: "Điều Ước Cuối Của Tù Nhân 2037: Một cái kết ẩn ý",
    detail:
      "Điều Ước Cuối Của Tù Nhân 2037 (이공삼칠) sở hữu câu chuyện không phải mới lạ, nhưng kỳ ở chỗ là nó vẫn cuốn hút, mang đến cảm giác dằn vặt khó tả, y như thứ cảm xúc mà nhân vật Yoon-yeong phải vượt qua",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/07/2037-3_KP-1920x1280.webp",
  },
  {
    url: "https://khenphim.com/trailer-the-matrix-resurrections/",
    title: "Huyền thoại “Ma Trận” hồi sinh",
    detail:
      "Gần hai thập kỷ sau khi bộ ba phim Ma Trận khép lại với sự ra đi của cặp đôi Neo (Keanu Reeves) – Trinity (Carrie-Anne Moss), đạo diễn Lana Wachowski đã quyết định mang thương hiệu thành công bậc nhất những năm đầu đầu thế kỷ 21 trở lại màn ảnh rộng.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2021/09/THE-MATRIX-RESURRECTIONS-1-teaser-poster.jpg",
  },
  {
    url: "https://khenphim.com/chi-chi-em-em-2/",
    title:
      "Chị Chị Em Em 2: Khi mỹ nhân lên tiếng, đàn ông sẽ quỳ rạp dưới chân",
    detail:
      "Chị Chị Em Em 2 có sự xuất hiện của Minh Hằng và Ngọc Trinh lần lượt trong vai Ba Trà và Tư Nhị đã khiến khán giả lập tức thắc mắc rằng liệu những người đẹp này có đủ sức làm nên thành công về mặt diễn xuất cho bộ phim hay không? Với những giai thoại đã biết về hai mỹ nhân nức tiếng Sài Thành đầu thế kỷ 20.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/02/Chi-Chi-Em-Em-2-2_KP.webp",
  },
  {
    url: "https://khenphim.com/take-care-of-my-mom/",
    title: "Hãy Chăm Sóc Mẹ: Lời nhắc nhở cho những ai còn mẹ",
    detail:
      "Hãy Chăm Sóc Mẹ xoay quanh một cụ bà 85 tuổi sống một mình ở vùng Daegu, Hàn Quốc. Bằng cách cho thuê nhà, bà vẫn có đồng ra đồng vào để tự do tiêu xài mà không cần nhờ đến sự trợ giúp của con cháu.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Take-Care-Of-My-Mom-4_KP.webp",
  },
  {
    url: "https://khenphim.com/strange-world/",
    title:
      "Strange World (Thế Giới Lạ Lùng): 3D và đồ họa đẹp, nội dung dễ hiểu",
    detail:
      "Strange World (Thế Giới Lạ Lùng) xoay quanh gia đình Clades với những thành viên đã làm nên huyền thoại trong làng phiêu lưu khám phá những vùng đất mới..",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Strange-World-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/puss-in-boots-the-last-wish/",
    title: "Review phim Mèo Đi Hia: Điều Ước Cuối Cùng",
    detail:
      "Mèo Đi Hia: Điều Ước Cuối Cùng (tên gốc: Puss In Boots: The Last Wish) kể về chuyến phiêu lưu đi tìm một ngôi sao có khả năng đặc biệt, nó sẽ ban cho bất kỳ ai một điều ước, bất cứ thứ gì mà họ nói ra sẽ thành hiện thực ngay..",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Puss-In-Boots-2022-2-Perrito_KP-e1671923386998.webp",
  },
  {
    url: "https://khenphim.com/operation-fortune/",
    title: " Phi Vụ Toàn Sao: Hành động đã mắt, tấu hài cực mạnh'",
    detail:
      "Phi Vụ Toàn Sao xoay quanh một phi vụ của điệp viên Orson Fortune (Jason Statham) cùng đồng đội lên kế hoạch bắt tận tay một gã chuyên buôn vũ khí trong thế giới ngầm.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Operation-Fortune_2.jpg",
  },
  {
    url: "https://khenphim.com/doctor-strange-in-the-multiverse-of-madness/",
    title:
      "Review phim Doctor Strange Phù Thủy Tối Thượng Trong Đa Vũ Trụ Hỗn Loạn",
    detail:
      "Doctor Strange vô tình sử dụng một phép thuật cấm, mở ra cánh cổng đa vũ trụ, khiến Strange phải đối mặt với một kẻ thù vừa lạ vừa quen, người đó tìm cách tiêu diệt các phù thủy để hòng đạt được mục tiêu cá nhân của mình.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/05/Doctor-Strange-madness-2-Dr-Strange_KP.webp",
  },
  {
    url: "https:www.zingnews.vn//lien-hoan-phim-busan-nong-vi-avatar-2-post1362665.html",
    title: "Liên hoan phim Busan nóng vì ‘Avatar 2’",
    detail:
      "Khán giả LHP Busan đã trả tiền để mua kính 3D và xem clip công chiếu sớm bom tấn lớn nhất 2022 của đạo diễn James Cameron.",
    img: "https://znews-photo.zingcdn.me/Uploaded/rohunaa/2022_10_06/wallpapersden.com_avatar_2_metkayina_village_2048x1113_1.jpg",
  },
  {
    url: "https://ghienreview.com/review-phim-avatar-2-mau-sac-ao-dieu-cua-thien-duong/",
    title: "Review phim Avatar 2: Màu sắc ảo diệu của thiên đường",
    detail:
      "Sau 13 năm, bộ phim có doanh thu cao nhất mọi thời đại mới công chiếu phần 2 và ngay lập tức Avatar 2 – Dòng chảy của nước đã tạo nên một cơn sốt trên khắp các phòng vé toàn cầu. Ở Việt Nam, rất khó để các bạn có thể book được một ghế có chỗ ngồi đẹp, thậm chí là book được 1 ghế tại một rạp phim bất kỳ vào khung giờ đẹp của bộ phim này, cho thấy sức hút khủng khiếp của Avatar lớn đến thế nào.",
    img: "https://ghienreview.com/wp-content/uploads/2022/12/17D6B5F9-6A7B-429F-92DD-2A3B588955B6.jpeg",
  },
  {
    url: "https://ghienreview.com/review-phim-thanh-guom-diet-quy-xem-xong-phai-tim-truyen-doc-lien/",
    title: "Thanh gươm diệt quỷ: Xem xong phải tìm truyện đọc liền",
    detail:
      "Bom tấn Anime đang làm mưa làm gió ở Nhật Bản và Đài Loan, đổ xô mọi kỷ lục phòng vé và là đối thủ nặng ký nhất của Khương Tử Nha (2020) ở vị trí phim hoạt hình ăn khách nhất năm 2020 đã chính thức được công chiếu tại các rạp ở Việt Nam.",
    img: "https://ghienreview.com/wp-content/uploads/2020/12/Ghien-review-Thanh-guom-diet-quy-Demon-Slayer-02-min.jpg",
  },
  {
    url: "https://khenphim.com/babylon/",
    title: "Babylon – Bức tranh đen tối nhưng chân thật về Hollywood một thời",
    detail:
      "Babylon lấy bối cảnh những năm 1920, khi Jack Conard (Brad Pitt vào vai) còn là nam diễn viên hạng A mà nhiều nhà sản xuất thèm khát, cũng như được khán giả mến mộ. Cùng thời điểm ấy, Nellie LaRoy (Margot Robbie) bắt đầu nổi lên như một nữ diễn viên triển vọng với khả năng diễn xuất tốt, kéo theo đó là anh chàng Manny Torres (Diego Calva) dần lấn sân vào thế giới điện ảnh dù trước đó chỉ là một anh chàng nài voi ít ai biết tới",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/02/Babylon-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/nha-ba-nu/",
    title: "Nhà Bà Nữ – Trọn vẹn cảm xúc, chạm tới trái tim khán giả",
    detail:
      "Nhà Bà Nữ dĩ nhiên là câu chuyện xoay quanh bà Nữ (Lê Giang vào vai) – một người phụ nữ bán bánh canh nhưng giọng chua hơn chanh và là người cai quản mọi việc lớn nhỏ trong nhà. Cái gia đình bất ổn một cách ổn định cho đến khi Nhi (Uyển Ân) có bạn trai, cũng như Như (Khả Như) và chồng (Trấn Thành) có những mâu thuẫn.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Nha-Ba-Nu-3-Le-Giang_KP.webp",
  },
  {
    url: "https://khenphim.com/where-the-crawdads-sing/",
    title: "Xa Ngoài Kia Nơi Loài Tôm Hát: Dữ dội như sóng ngầm",
    detail:
      "Xa Ngoài Kia Nơi Loài Tôm Hát (tên gốc: Where The Crawdads Sing) thu hút khán giả bởi phim được chuyển thể từ cuốn tiểu thuyết cùng tên (tác giả Delia Owens) cũng như có bài hát gốc của Taylor Swift.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/08/Where-The-Crawdads-Sing-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/black-adam/",
    title: "Black Adam: Đẹp mắt, đã tai và nội dung bất ngờ",
    detail:
      "Black Adam kể về một nhân vật cùng tên của nhà DC. Dwayne Johnson hay còn được biết đến dưới cái tên The Rock sẽ thủ vai chính, dưới sự sáng tạo của đạo diễn Jaume Collet-Serra.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/10/Black-Adam-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/jailangkung-sandekala/",
    title: "Jailangkung: Sandekala",
    detail:
      "Jailangkung: Búp Bê Gọi Hồn kể về một gia đình nọ gồm 4 người: vợ, chồng, con gái, con trai út. Trong chuyến đi chơi cạnh hồ nước, cậu con út bị lạc vào rừng rồi mất tích một cách bí ẩn. Dù huy động cả một lực lượng tìm kiếm chuyên nghiệp nhưng vẫn không thể tìm ra tung tích cậu bé. Người dân địa phương đồn đoán rằng khu vực quanh hồ bị ám, đã từng có rất nhiều trẻ em mất tích...",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Jailangkung-Sandekala-3_KP.webp",
  },
  {
    url: "https://khenphim.com/2037/",
    title: "Điều Ước Cuối Của Tù Nhân 2037: Một cái kết ẩn ý",
    detail:
      "Điều Ước Cuối Của Tù Nhân 2037 (이공삼칠) sở hữu câu chuyện không phải mới lạ, nhưng kỳ ở chỗ là nó vẫn cuốn hút, mang đến cảm giác dằn vặt khó tả, y như thứ cảm xúc mà nhân vật Yoon-yeong phải vượt qua",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/07/2037-3_KP-1920x1280.webp",
  },
  {
    url: "https://khenphim.com/trailer-the-matrix-resurrections/",
    title: "Huyền thoại “Ma Trận” hồi sinh",
    detail:
      "Gần hai thập kỷ sau khi bộ ba phim Ma Trận khép lại với sự ra đi của cặp đôi Neo (Keanu Reeves) – Trinity (Carrie-Anne Moss), đạo diễn Lana Wachowski đã quyết định mang thương hiệu thành công bậc nhất những năm đầu đầu thế kỷ 21 trở lại màn ảnh rộng.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2021/09/THE-MATRIX-RESURRECTIONS-1-teaser-poster.jpg",
  },
  {
    url: "https://khenphim.com/chi-chi-em-em-2/",
    title:
      "Chị Chị Em Em 2: Khi mỹ nhân lên tiếng, đàn ông sẽ quỳ rạp dưới chân",
    detail:
      "Chị Chị Em Em 2 có sự xuất hiện của Minh Hằng và Ngọc Trinh lần lượt trong vai Ba Trà và Tư Nhị đã khiến khán giả lập tức thắc mắc rằng liệu những người đẹp này có đủ sức làm nên thành công về mặt diễn xuất cho bộ phim hay không? Với những giai thoại đã biết về hai mỹ nhân nức tiếng Sài Thành đầu thế kỷ 20.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/02/Chi-Chi-Em-Em-2-2_KP.webp",
  },
  {
    url: "https://khenphim.com/take-care-of-my-mom/",
    title: "Hãy Chăm Sóc Mẹ: Lời nhắc nhở cho những ai còn mẹ",
    detail:
      "Hãy Chăm Sóc Mẹ xoay quanh một cụ bà 85 tuổi sống một mình ở vùng Daegu, Hàn Quốc. Bằng cách cho thuê nhà, bà vẫn có đồng ra đồng vào để tự do tiêu xài mà không cần nhờ đến sự trợ giúp của con cháu.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Take-Care-Of-My-Mom-4_KP.webp",
  },
  {
    url: "https://khenphim.com/strange-world/",
    title:
      "Strange World (Thế Giới Lạ Lùng): 3D và đồ họa đẹp, nội dung dễ hiểu",
    detail:
      "Strange World (Thế Giới Lạ Lùng) xoay quanh gia đình Clades với những thành viên đã làm nên huyền thoại trong làng phiêu lưu khám phá những vùng đất mới..",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Strange-World-1-poster_KP.webp",
  },
  {
    url: "https://khenphim.com/puss-in-boots-the-last-wish/",
    title: "Review phim Mèo Đi Hia: Điều Ước Cuối Cùng",
    detail:
      "Mèo Đi Hia: Điều Ước Cuối Cùng (tên gốc: Puss In Boots: The Last Wish) kể về chuyến phiêu lưu đi tìm một ngôi sao có khả năng đặc biệt, nó sẽ ban cho bất kỳ ai một điều ước, bất cứ thứ gì mà họ nói ra sẽ thành hiện thực ngay..",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/12/Puss-In-Boots-2022-2-Perrito_KP-e1671923386998.webp",
  },
  {
    url: "https://khenphim.com/operation-fortune/",
    title: " Phi Vụ Toàn Sao: Hành động đã mắt, tấu hài cực mạnh'",
    detail:
      "Phi Vụ Toàn Sao xoay quanh một phi vụ của điệp viên Orson Fortune (Jason Statham) cùng đồng đội lên kế hoạch bắt tận tay một gã chuyên buôn vũ khí trong thế giới ngầm.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2023/01/Operation-Fortune_2.jpg",
  },
  {
    url: "https://khenphim.com/doctor-strange-in-the-multiverse-of-madness/",
    title:
      "Review phim Doctor Strange Phù Thủy Tối Thượng Trong Đa Vũ Trụ Hỗn Loạn",
    detail:
      "Doctor Strange vô tình sử dụng một phép thuật cấm, mở ra cánh cổng đa vũ trụ, khiến Strange phải đối mặt với một kẻ thù vừa lạ vừa quen, người đó tìm cách tiêu diệt các phù thủy để hòng đạt được mục tiêu cá nhân của mình.",
    img: "https://khenphim.b-cdn.net/wp-content/uploads/2022/05/Doctor-Strange-madness-2-Dr-Strange_KP.webp",
  },
  {
    url: "https:www.zingnews.vn//lien-hoan-phim-busan-nong-vi-avatar-2-post1362665.html",
    title: "Liên hoan phim Busan nóng vì ‘Avatar 2’",
    detail:
      "Khán giả LHP Busan đã trả tiền để mua kính 3D và xem clip công chiếu sớm bom tấn lớn nhất 2022 của đạo diễn James Cameron.",
    img: "https://znews-photo.zingcdn.me/Uploaded/rohunaa/2022_10_06/wallpapersden.com_avatar_2_metkayina_village_2048x1113_1.jpg",
  },
];
